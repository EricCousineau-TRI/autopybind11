# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt
import os
import sys
import unittest

# Make it so we search where we are running.
sys.path.append(os.getcwd())  # noqa

import additional
# Import of test_base_double_py from additional causes conflict on run
# from additional.additional import nonTemplate_py


class additionalTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def test_test_base_double_py(self):
        tb = additional.test_base_double_py()
        self.assertIn("mult", dir(tb))
        tb.base_var = 2
        self.assertEqual(6, tb.mult(3))

    def test_nonTemplate(self):
        nt = additional.nonTemplate_py()
        self.assertIn("div", dir(nt))
        self.assertIn("fdiv", dir(nt))

        self.assertEqual(6, nt.div(12, 2))
        self.assertAlmostEqual(6, nt.fdiv(18, 3))
        self.assertAlmostEqual(5.90322589874267, nt.fdiv(18.3, 3.1))


if __name__ == '__main__':
    unittest.main()
