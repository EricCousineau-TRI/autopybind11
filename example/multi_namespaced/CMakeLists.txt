# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt

add_library(multi_namespaced INTERFACE)
target_sources(multi_namespaced INTERFACE
  ${CMAKE_CURRENT_SOURCE_DIR}/no_namespace.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/first_namespace.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/second_namespace.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/compound_namespace.hpp)
target_include_directories(multi_namespaced INTERFACE
  ${CMAKE_CURRENT_SOURCE_DIR})

autopybind11_fetch_build_pybind11()

autopybind11_add_module("multi_namespaced_module"
                       YAML_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/wrapper_input.yml
                       CONFIG_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/config.yml
                       DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
                       LINK_LIBRARIES multi_namespaced
                       )

add_test(NAME namespace_unittest
         COMMAND ${PYTHON_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/tests/testMultiNamespaced.py
         WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})

