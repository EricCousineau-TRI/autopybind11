# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt

import os
import sys
import unittest

# Make it so we search where we are running.
sys.path.append(os.getcwd())  # noqa

import multi_target as mt
# Import of test_base_double_py from additional causes conflict on run
# from additional.additional import nonTemplate_py


class multiTargetTests(unittest.TestCase):
    def test_flags(self):
        mt.first_target_class_py().flag1_defined()
        mt.second_target_class_py().flag2_defined()
        mt.second_target_class_py().flag3_defined()

    def test_free_fxn(self):
        mt.first_target_fxn()

    def test_include(self):
        mt.first_target_class_py().call_free_fxn()


if __name__ == '__main__':
    unittest.main()
