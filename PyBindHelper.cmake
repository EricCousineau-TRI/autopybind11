# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt

macro(autopybind11_fetch_build_pybind11)

    include(FetchContent)
    FetchContent_Declare(
      pybind11
      GIT_REPOSITORY https://github.com/pybind/pybind11
      GIT_TAG        v2.5.0
      )
    set(FETCHCONTENT_QUIET OFF)
    FetchContent_MakeAvailable(pybind11)
    include_directories(${Python_INCLUDE_DIRS})

    find_program(CastXML_EXECUTABLE NAMES castxml castxml.exe)
    if(NOT CastXML_EXECUTABLE)
      message(FATAL_ERROR
        "Required program castxml not found set CastXML_EXECUTABLE")
    endif()

    set(AutoPyBind11_generator ${PYTHON_EXECUTABLE} -m autopybind11)
endmacro()

function(_autopybind11_get_compiler_flags)
    set(generateSingleValueArgs INCLUDE_FLAGS DEFINE_FLAGS)
    set(generateMultiValueArgs TARGETS )
    cmake_parse_arguments(PYBIND_GET_FLAGS "" "${generateSingleValueArgs}" "${generateMultiValueArgs}" ${ARGN})
    list(LENGTH PYBIND_GET_FLAGS_TARGETS num_targets)
    set(includes_list)
    set(defines_list)
    if(${num_targets} GREATER 0)
        foreach(target ${PYBIND_GET_FLAGS_TARGETS})
          list(APPEND
               includes_list
               $<TARGET_PROPERTY:${target},INTERFACE_INCLUDE_DIRECTORIES>)

          list(APPEND
               defines_list
               $<TARGET_PROPERTY:${target},INTERFACE_COMPILE_DEFINITIONS>)
        endforeach()
        set(${PYBIND_GET_FLAGS_INCLUDE_FLAGS} "${includes_list}"  PARENT_SCOPE)
        set(${PYBIND_GET_FLAGS_DEFINE_FLAGS} "${defines_list}" PARENT_SCOPE)
    endif()
endfunction()

function(_autopybind11_get_sources)
    set(PreProcessOneValueArgs YAML_INPUT DESTINATION GENERATED_SOURCES)
    set(PYBIND_RUN_NAME ${ARGV0})
    cmake_parse_arguments(PARSE_ARGV 1 PYBIND_RUN "" "${PreProcessOneValueArgs}" "")
    execute_process(COMMAND ${AutoPyBind11_generator} "-y"  ${PYBIND_RUN_YAML_INPUT}
                                                                           "-o"  ${PYBIND_RUN_DESTINATION}
                                                                           "--module_name" "${PYBIND_RUN_NAME}"
                                                                           -n
                OUTPUT_VARIABLE sources OUTPUT_STRIP_TRAILING_WHITESPACE)
    set(${PYBIND_RUN_GENERATED_SOURCES} ${sources} PARENT_SCOPE)
endfunction()

function(autopybind11_add_module)
    if(NOT DEFINED AutoPyBind11_generator)
      message(FATAL_ERROR "autopybind11_fetch_build_pybind11 must be called first")
    endif()
    set(generateOneValueArgs YAML_INPUT CONFIG_INPUT DESTINATION NAMESPACE C_STD_FLAG)
    set(generateMultiValueArgs LINK_LIBRARIES FILES)
    set(PYBIND_ADD_LIB_NAME ${ARGV0})

    cmake_parse_arguments(PARSE_ARGV 1 PYBIND_ADD_LIB "" "${generateOneValueArgs}" "${generateMultiValueArgs}")
    if(NOT PYBIND_ADD_LIB_YAML_INPUT)
      message(FATAL_ERROR "YAML_INPUT must be specified")
    endif()
    if(NOT PYBIND_ADD_LIB_LINK_LIBRARIES)
      message(FATAL_ERROR "LINK_LIBRARIES must be specified")
    endif()
    if(NOT PYBIND_ADD_LIB_C_STD_FLAG)
      set(PYBIND_ADD_LIB_C_STD_FLAG "-std=c++14")
    endif()
    _autopybind11_get_sources(${PYBIND_ADD_LIB_NAME} YAML_INPUT ${PYBIND_ADD_LIB_YAML_INPUT}
                           DESTINATION ${PYBIND_ADD_LIB_DESTINATION}
                           GENERATED_SOURCES PYBIND_ADD_LIB_FILES)
    if (PYBIND_ADD_LIB_NAMESPACE)
      set(DEFAULT_NAMESPACE "-d" "${PYBIND_ADD_LIB_NAMESPACE}")
    endif()
    _autopybind11_get_compiler_flags(TARGETS ${PYBIND_ADD_LIB_LINK_LIBRARIES}
        INCLUDE_FLAGS includes DEFINE_FLAGS defines)

    # Get name of file which should correspond directly to name of python module created in it
    # The first name in the list should be the overall module
    list(GET PYBIND_ADD_LIB_FILES 0 file)
    get_filename_component(tgt_helper_name ${file} NAME_WE)

    # Add custom target to generate the code
    # Add depends on wrapper_input.YAML and header files?

    # Generate file
    set(PYBIND_ADD_LIB_RESPONSE_PATH "${CMAKE_CURRENT_BINARY_DIR}/response.rsp")

    file(GENERATE OUTPUT "${PYBIND_ADD_LIB_RESPONSE_PATH}"
      CONTENT "includes: ${includes}\nc_std: ${PYBIND_ADD_LIB_C_STD_FLAG}\ndefines: ${defines}")

    if(PYBIND_ADD_LIB_CONFIG_INPUT AND EXISTS ${PYBIND_ADD_LIB_CONFIG_INPUT})
        add_custom_command(OUTPUT ${PYBIND_ADD_LIB_FILES} wrapper.cpp COMMAND ${AutoPyBind11_generator}
                                                                          -y ${PYBIND_ADD_LIB_YAML_INPUT}
                                                                          "--module_name" ${PYBIND_ADD_LIB_NAME}
                                                                          "-g"  ${CastXML_EXECUTABLE}
                                                                          "-cg" "${PYBIND_ADD_LIB_CONFIG_INPUT}"
                                                                          "-rs" "${PYBIND_ADD_LIB_RESPONSE_PATH}"
                                                                          "-o"  ${PYBIND_ADD_LIB_DESTINATION}
                                                                          ${DEFAULT_NAMESPACE}
                                                                          DEPENDS ${PYBIND_ADD_LIB_YAML}
                                                                          ${PYBIND_ADD_LIB_CONFIG_INPUT}
                                                                          ${PYBIND_ADD_LIB_RESPONSE_PATH}
                                                                        WORKING_DIRECTORY ${PYBIND_ADD_LIB_DESTINATION})

    else()
      add_custom_command(OUTPUT ${PYBIND_ADD_LIB_FILES} wrapper.cpp
        COMMAND ${AutoPyBind11_generator} -y ${PYBIND_ADD_LIB_YAML_INPUT}
                                                                          "--module_name" ${PYBIND_ADD_LIB_NAME}
                                                                          "-g"  ${CastXML_EXECUTABLE}
                                                                          "-o"  ${PYBIND_ADD_LIB_DESTINATION}
                                                                          "-rs" ${PYBIND_ADD_LIB_RESPONSE_PATH}
                                                                          ${DEFAULT_NAMESPACE}
                                                                          DEPENDS ${PYBIND_ADD_LIB_YAML}
                                                                          ${PYBIND_ADD_LIB_RESPONSE_PATH}
                                                                          WORKING_DIRECTORY ${PYBIND_ADD_LIB_DESTINATION})
    endif()
    # Create the module with that file as it's only source
    pybind11_add_module(${tgt_helper_name} ${PYBIND_ADD_LIB_FILES})
    target_link_libraries(${tgt_helper_name} PUBLIC ${PYBIND_ADD_LIB_LINK_LIBRARIES})
    target_include_directories(${tgt_helper_name} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}  ${PYBIND_ADD_LIB_DESTINATION})
    # target_compile_definitions(${tgt_helper_name} PUBLIC "${flags}")
endfunction()